from django.db import models
from datetime import *
# Create your models here.
class Post(models.Model):
    nama        = models.CharField(max_length = 20)
    hari        = models.CharField(max_length = 10)
    tempat      = models.CharField(max_length = 20)
    kategori    = models.CharField(max_length = 20)
    tanggal     = models.DateTimeField(default = datetime.now)
    jam         = models.TimeField(max_length = 20)

    def __str__(self):
        return "{}.{}".format(self.nama, self.hari)