from django.shortcuts import render, redirect
from .todoForm import TodoForm
from .models import Post

def delete(request, delete_id):
    Post.objects.filter(id=delete_id).delete()
    return redirect('todo')

def index(request):
    posts = Post.objects.all()
    context = {
        'posts' : posts,
    }

    return render(request, 'todo.html', context)

def create(request):
    todo_form = TodoForm(request.POST or None)
    if request.method == 'POST':
        if todo_form.is_valid():
            todo_form.save()

            return redirect('todo')

    context = {
        'todo_form' : todo_form
    }

    return render(request,'create.html',context)