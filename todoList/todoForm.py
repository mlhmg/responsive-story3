from django import forms
from .models import Post

class TodoForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = [
            'nama',
            'hari',
            'tempat',
            'kategori',
            'tanggal',
            'jam',
        ]
        widgets = {
            'nama' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'ngedota'}
                ),
            'kategori': forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'Kuliah'}
                ),
            'tempat' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'Kampus'}
                ),
            'hari' : forms.TextInput(attrs={
                'class' : 'form-control',
                'placeholder':'Senin'}
                ),
            'tanggal' : forms.DateInput(attrs={
                'class' : 'form-control',
                'type':'date'}),
            'jam' : forms.TimeInput(attrs={
                'class' : 'form-control',
                'type':'time'}),
        }

    