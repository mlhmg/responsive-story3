from django.urls import path
from . import views

appname = 'todoList'

urlpatterns = [
    path('', views.index, name='todo'),
    path('delete/(?P<delete_id>{0-9})', views.delete, name='delete'),
    path('create/', views.create, name='create'),
]