from django.shortcuts import render
from todoList.views import index as todoList_index

def index(request):
    return render(request, 'index.html')
def experience(request):
    return render(request, 'experience.html')
def projects(request):
    return render(request, 'projects.html')
def skills(request):
    return render(request, 'skills.html')
# def todoList(request):
    # return todoList(request)
# Create your views here.
